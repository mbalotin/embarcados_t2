
Code Execution Report - Core 3

CPU cycles: 75000001
WCET: 3000.0000ms
Estimated energy consumption: 0.127254J (20587 gates Plasma CPU core, CMOS TSMC 0.35um)

Instructions (MIPS I instruction set):
       J:    3603969;      JAL:   10767314;      BEQ:    3525718;      BNE:     340646; 
    BLEZ:        638;     BGTZ:          0;     ADDI:       4286;    ADDIU:     469828; 
    SLTI:         28;    SLTIU:        334;     ANDI:      94429;      ORI:       2703; 
    XORI:        278;      LUI:     106827;     COP0:      21411;     BEQL:          0; 
    BNEL:          0;    BLEZL:          0;    BGTZL:          0;       LB:       4521; 
      LH:          0;       LW:    7153444;      LBU:       2889;      LHU:     138522; 
      SB:     125471;       SH:      80073;       SW:     230782;       LL:          0; 
      SC:          0;      SLL:     112902;      SRL:     172695;      SRA:         82; 
    SLLV:         96;     SRLV:          0;     SRAV:          0;       JR:   10770806; 
    JALR:       1988;     MOVZ:          0;     MOVN:          0;     MFHI:      10313; 
    MTHI:       1428;     MFLO:       1748;     MTLO:       1428;     MULT:          0; 
   MULTU:          0;      DIV:       8637;     DIVU:        247;      ADD:          0; 
    ADDU:     187619;      SUB:          0;     SUBU:      21052;      AND:       1678; 
      OR:      83647;      XOR:          0;      NOR:          0;      SLT:     129526; 
    SLTU:      40943;    DADDU:          0;     BLTZ:        288;     BGEZ:      16402; 
   BLTZL:          0;    BGEZL:          0;   BLTZAL:          0;   BGEZAL:          0; 
 BLTZALL:          0;  BGEZALL:          0; 

Instructions executed: 38237636
Effective instructions per cycle (IPC): 0.509835
I/O wait cycles: 0

Instructions executed from each class:
Arithmetic:        862500 (2.255631%)
Branch/Jump:     29027769 (75.914124%)
Load/Store:       7735702 (20.230597%)
Logical:           289562 (0.757270%)
Move:               36328 (0.095006%)
Shift:             285775 (0.747366%)

Flits sent: 0
Flits received: 0
Broadcasts: 0
