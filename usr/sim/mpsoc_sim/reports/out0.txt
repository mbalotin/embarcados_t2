
KERNEL: booting...
===========================================================
HellfireOS v2.17.03 (4.6.1) [Oct 11 2017, 20:11:42]
Embedded Systems Group - GSE, PUCRS - [2007 - 2017]
===========================================================

arch:          mips/plasma
sys clk:       25000 kHz
time slice:    10480 us
heap size:     500000 bytes
max tasks:     30

HAL: _vm_init()
HAL: _sched_init()
HAL: _timer_init()
HAL: _irq_init()
KERNEL: [idle task], id: 0, p:0, c:0, d:0, addr: 10001034, sp: 10005580, ss: 1024 bytes
HAL: _device_init()
KERNEL: this is core 0
KERNEL: NoC queue init, 64 packets
KERNEL: NoC driver registered
HAL: _task_init()
KERNEL: [receiver], id: 1, p:0, c:0, d:0, addr: 10002418, sp: 100088b0, ss: 4096 bytes
KERNEL: free heap: 485368 bytes
KERNEL: HellfireOS is up
