#include <hellfire.h>
#include <noc.h>

void task1(void){
	int16_t val;

	if (hf_comm_create(hf_selfid(), 1000, 0))
		panic(0xff);
    while (1){

	    char buf_t2[256];
	    sprintf(buf_t2, "t1>t2\n");
	    val = hf_sendack(1, 1001, buf_t2, sizeof(buf_t2), 1, 500);
	    printf("Task1 sent to Task2\n", val);

	    char buf_t3[64];
	    sprintf(buf_t3, "t1>t3\n");
	    val = hf_sendack(2, 1002, buf_t3, sizeof(buf_t3), 1, 500);
	    printf("Task1 sent to Task3\n", val);

	    char buf_t4[64];
	    sprintf(buf_t4, "t1>t4\n");
	    val = hf_sendack(3, 1003, buf_t4, sizeof(buf_t4), 1, 500);
	    printf("Task1 sent to Task4\n", val);

	    char buf_t5[64];
	    sprintf(buf_t5, "t1>t5\n");
	    val = hf_sendack(4, 1004, buf_t5, sizeof(buf_t5), 1, 500);
	    printf("Task1 sent to Task5\n", val);

	    char buf_t7[1280];
	    sprintf(buf_t7, "t1>t7\n");
	    val = hf_sendack(5, 1006, buf_t7, sizeof(buf_t7), 1, 500);
	    printf("Task1 sent to Task7\n", val);

	    printf("Task1 sent all messages\n", val);

        delay_ms(500);
    }
}

void task2(void){
	char buf_t1[256];
	uint16_t cpu, task, size;
	int16_t val;

	if (hf_comm_create(hf_selfid(), 1001, 0))
		panic(0xff);

    while (1){
	    val = hf_recvack(&cpu, &task, buf_t1, &size, 1);
	    if (val)
		    printf("hf_recvack(): error %d\n", val);
	    else
		    printf("%s", buf_t1);

	    char buf_t6[64];
	    sprintf(buf_t6, "t2>t6\n");
	    val = hf_sendack(2, 1005, buf_t6, sizeof(buf_t6), 2, 500);
	    printf("Task2 sent to Task6\n", val);

	    char buf_t7[320];
	    sprintf(buf_t7, "t2>t7\n");
	    val = hf_sendack(5, 1006, buf_t7, sizeof(buf_t7), 2, 500);
	    printf("Task2 sent to Task7\n", val);

	    char buf_t8[320];
	    sprintf(buf_t8, "t2>t8\n");
	    val = hf_sendack(0, 1007, buf_t8, sizeof(buf_t8), 2, 500);
	    printf("Task2 sent to Task8\n", val);
    }

}

void task3(void){
	char buf_t1[64];
	uint16_t cpu, task, size;
	int16_t val;

	if (hf_comm_create(hf_selfid(), 1002, 0))
		panic(0xff);

    while (1){
	    val = hf_recvack(&cpu, &task, buf_t1, &size, 1);
	    if (val)
		    printf("hf_recvack(): error %d\n", val);
	    else
		    printf("%s", buf_t1);

	    char buf_t7[320];
	    sprintf(buf_t7, "t3>t7\n");
	    val = hf_sendack(5, 1006, buf_t7, sizeof(buf_t7), 3, 500);
	    printf("Task3 sent to Task7\n", val);

	    char buf_t8[64];
	    sprintf(buf_t8, "t3>t8\n");
	    val = hf_sendack(0, 1007, buf_t8, sizeof(buf_t8), 3, 500);
	    printf("Task3 sent to Task8\n", val);
    }
}

void task4(void){
	char buf_t1[64];
	uint16_t cpu, task, size;
	int16_t val;

	if (hf_comm_create(hf_selfid(), 1003, 0))
		panic(0xff);

    while (1){
	    val = hf_recvack(&cpu, &task, buf_t1, &size, 1);
	    if (val)
		    printf("hf_recvack(): error %d\n", val);
	    else
		    printf("%s", buf_t1);

	    char buf_t8[64];
	    sprintf(buf_t8, "t4>t8\n");
	    val = hf_sendack(0, 1007, buf_t8, sizeof(buf_t8), 4, 500);
	    printf("Task4 sent to Task8\n", val);
    }
}

void task5(void){
	char buf_t1[64];
	uint16_t cpu, task, size;
	int16_t val;

	if (hf_comm_create(hf_selfid(), 1004, 0))
		panic(0xff);
    while (1){
	    val = hf_recvack(&cpu, &task, buf_t1, &size, 1);
	    if (val)
		    printf("hf_recvack(): error %d\n", val);
	    else
		    printf("%s", buf_t1);

	    char buf_t8[640];
	    sprintf(buf_t8, "t5>t8\n");
	    val = hf_sendack(0, 1007, buf_t8, sizeof(buf_t8), 5, 500);
	    printf("Task5 sent to Task8\n", val);
    }
}

void task6(void){
	char buf_t2[64];
	uint16_t cpu, task, size;
	int16_t val;

	if (hf_comm_create(hf_selfid(), 1005, 0))
		panic(0xff);
    while (1){
	    val = hf_recvack(&cpu, &task, buf_t2, &size, 2);
	    if (val)
		    printf("hf_recvack(): error %d\n", val);
	    else
		    printf("%s", buf_t2);

	    char buf_t9[640];
	    sprintf(buf_t9, "t6>t9\n");
	    val = hf_sendack(1, 1008, buf_t9, sizeof(buf_t9), 6, 500);
	    printf("Task6 sent to Task9\n", val);
    }
}

void task7(void){
	uint16_t cpu, task, size;
	int16_t val;

	if (hf_comm_create(hf_selfid(), 1006, 0))
		panic(0xff);
    while (1){
	    char buf_t1[1280];
	    val = hf_recvack(&cpu, &task, buf_t1, &size, 1);
	    if (val) printf("hf_recvack(): error %d\n", val);
	    else printf("%s", buf_t1);

	    char buf_t2[320];
	    val = hf_recvack(&cpu, &task, buf_t2, &size, 2);
	    if (val) printf("hf_recvack(): error %d\n", val);
	    else printf("%s", buf_t2);

	    char buf_t3[320];
	    val = hf_recvack(&cpu, &task, buf_t3, &size, 3);
	    if (val) printf("hf_recvack(): error %d\n", val);
	    else printf("%s", buf_t3);

	    char buf_t9[640];
	    sprintf(buf_t9, "t7>t9\n");
	    val = hf_sendack(1, 1008, buf_t9, sizeof(buf_t9), 7, 500);
	    printf("Task7 sent to Task9\n", val);
    }
}

void task8(void){
	uint16_t cpu, task, size;
	int16_t val;

	if (hf_comm_create(hf_selfid(), 1007, 0))
		panic(0xff);
    while (1){
	    char buf_t2[320];
	    val = hf_recvack(&cpu, &task, buf_t2, &size, 2);
	    if (val) printf("hf_recvack(): error %d\n", val);
	    else printf("%s", buf_t2);

	    char buf_t3[64];
	    val = hf_recvack(&cpu, &task, buf_t3, &size, 3);
	    if (val) printf("hf_recvack(): error %d\n", val);
	    else printf("%s", buf_t3);

	    char buf_t4[64];
	    val = hf_recvack(&cpu, &task, buf_t4, &size, 4);
	    if (val) printf("hf_recvack(): error %d\n", val);
	    else printf("%s", buf_t4);

	    char buf_t5[640];
	    val = hf_recvack(&cpu, &task, buf_t5, &size, 5);
	    if (val) printf("hf_recvack(): error %d\n", val);
	    else printf("%s", buf_t5);

	    char buf_t9[640];
	    sprintf(buf_t9, "t8>t9\n");
	    val = hf_sendack(1, 1008, buf_t9, sizeof(buf_t9), 8, 500);
	    printf("Task8 sent to Task9\n", val);
    }
}

void task9(void){
	uint16_t cpu, task, size;
	int16_t val;

	if (hf_comm_create(hf_selfid(), 1008, 0))
		panic(0xff);
    while (1){
	    char buf_t6[640];
	    val = hf_recvack(&cpu, &task, buf_t6, &size, 6);
	    if (val) printf("hf_recvack(): error %d\n", val);
	    else printf("%s", buf_t6);

	    char buf_t7[640];
	    val = hf_recvack(&cpu, &task, buf_t7, &size, 7);
	    if (val) printf("hf_recvack(): error %d\n", val);
	    else printf("%s", buf_t7);

	    char buf_t8[640];
	    val = hf_recvack(&cpu, &task, buf_t8, &size, 8);
	    if (val) printf("hf_recvack(): error %d\n", val);
	    else printf("%s", buf_t8);

	    printf("All tasks completed.\n", val);
    }
}

void app_main(void)
{
	if (hf_cpuid() == 0){
		hf_spawn(task1, 0, 0, 0, "t1", 4096);
		hf_spawn(task8, 0, 0, 0, "t8", 4096);
	}if (hf_cpuid() == 1){
		hf_spawn(task2, 0, 0, 0, "t2", 4096);
		hf_spawn(task9, 0, 0, 0, "t9", 4096);
	}if (hf_cpuid() == 2){
		hf_spawn(task3, 0, 0, 0, "t3", 4096);
		hf_spawn(task6, 0, 0, 0, "t6", 4096);
	}if (hf_cpuid() == 3){
		hf_spawn(task4, 0, 0, 0, "t4", 4096);
	}if (hf_cpuid() == 4){
		hf_spawn(task5, 0, 0, 0, "t5", 4096);
	}if (hf_cpuid() == 5){
		hf_spawn(task7, 0, 0, 0, "t7", 4096);
	}
}
